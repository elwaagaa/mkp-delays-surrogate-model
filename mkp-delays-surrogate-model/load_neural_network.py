"""
Script to load trained neural network to make predictions 
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import torch
from torch import nn
import pickle

from neural_network_with_validation import MLP
n_features = 9  #if we use our own data 

use_own_data = True #use own data (dim(X) = 923x9), or Boston dataset from scikit learn (dim(X) = 506x13)
normalised_input = True  #to normalise input from [-1, 1] --> [0, 1]
load_only_kept_beam = False #load data only where the beam was not lost (no high penalty plateau)

#Set some initial parameters
n_epochs = 200
batch_size = 64  #originally 64
learning_rate = 4e-4 
torch.manual_seed(4)    # so is reproducible

if use_own_data == True:
    n_features = 9
    scale_data_bool = normalised_input
    if normalised_input:
        save_str = 'mkp_delay_model_normalised_input'
    else:    
        save_str = 'mkp_delay_model'
    plt_str = 'MKP delays neural network'
else:
    n_features = 13
    scale_data_bool = True
    save_str = 'boston_model'
    plt_str = 'Boston housing model'


model = MLP(n_features)
model.load_state_dict(torch.load("nn_models/{}_{}_training_epochs.pth".format(save_str, n_epochs))

x_raw = np.zeros(9).reshape(-1)
x = torch.from_numpy(x_raw).float()

with torch.no_grad():
    pred = model(x)