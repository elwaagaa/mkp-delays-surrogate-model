"""
Optimisation script for trained neural network with MKP kicker delays from SPS  
--> generates many initial conditions and tries it for different optimisers 

Uses nn models whose input are normalised from [-1, 1] --> [0, 1], and whose output is normalised of the logarithm to [0, 1]
"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import torch
from torch import nn
import pickle
from scipy import optimize
import pybobyqa
from bayes_opt import BayesianOptimization
from neural_network_with_validation import MLP


#Choose model to load depending on #training_epochs
n_epochs = 200  #choose number of epochs of model to load from
n_features = 9  #mkp delays: 8 individual shift, 1 general shift 
use_normalised_input = True
use_trained_only = False #use nn models which have been fully trained on the full dataset, not validated
max_iter = 375  #maximum number of iterations for each optimiser  
n_t = 100  #total number of IC:s to test
saveplot = True

#Choose correct string to load model 
high_bound = 1.
if use_normalised_input :
    norm_str = '_normalised_input'
    low_bound = 0.
else: 
    norm_str = ''   
    low_bound = -1
if use_trained_only:
    train_str = '_training_only'
else:
    train_str = ''
load_str = 'mkp_delay_model{}{}'.format(norm_str, train_str)


#Initialize and load the model 
dof = 9 #all 8 individual shifts + general shift 
model = MLP(n_features)
model.load_state_dict(torch.load("nn_models/{}_{}_training_epochs.pth".format(load_str, n_epochs)))

#Create initial conditions sampled from a normal distribution centered around, then create IC vectors 
mean = (high_bound - np.abs(low_bound))/2
std = 0.1
Ns = [] #arrays for all normal distributions for which each MKP delay initial condition is generated (9 in total)
dtaus = []  #array for initial conditions 
for i in range(n_features):  
    N = np.random.normal(loc=mean, scale=std, size=n_t)
    Ns.append(N)
Ns = np.array(Ns)


# Define function to minimise, converting floats to tensors for the model 
# also generates plots and saves data
def minimizing_objective(dtau):
    if not torch.is_tensor(dtau):
        actions = torch.from_numpy(dtau).float()  #convert to tensor
    model.eval()
    with torch.no_grad():
        actions = torch.unsqueeze(actions, 0)  #unsqueeze to right dimensions
        loss = model(actions)  #evaluate with trained model 
    return loss.item()

#Also define objective function for Bayesian, that maxmises the rewards
def bayesian_objective(dtau1, dtau2, dtau3, dtau4, dtau5, dtau6, dtau7, dtau8, dtau9):
    dtau = np.array([dtau1, dtau2, dtau3, dtau4, dtau5, dtau6, dtau7, dtau8, dtau9])
    if not torch.is_tensor(dtau):
        actions = torch.from_numpy(dtau).float()  #convert to tensor
    model.eval()
    with torch.no_grad():
        actions = torch.unsqueeze(actions, 0)  #unsqueeze to right dimensions
        reward = -1*model(actions)  #evaluate with trained model 
    return reward.item()

#Initiate action and penalty 
actions_B = []
penalty_B = []
actions_NM = []
penalty_NM = []
actions_P = []
penalty_P = []
actions_Bay = []
penalty_Bay = []

#Define bounds and limits for the different algorithms
upper_lim = np.ones(dof)
lower_lim = low_bound * np.ones(dof)
bounds = (lower_lim, upper_lim)
upper_lim_2 = np.ones(9)
lower_lim_2 = low_bound*np.ones(9)
bounds_2 = tuple(zip(lower_lim_2, upper_lim_2))
p_bounds = {f"dtau{ele}": (low_bound, 1) for ele in range(1, dof+1)} #Bayesian optimisation 


#Iterate over all the possible initial conditions 
for i in range(n_t):
    print("\nCommencing IC iteration {} out of {}...".format(i+1, n_t))
    
    dtau0 = Ns[:, i]
    
    # ------------ Optimize with PyBOBYQA ----------------------------------------
    #If not possible with initial conditions, add high penalty 
    try:  
        results = pybobyqa.solve(
            minimizing_objective,
            dtau0,
            bounds=bounds,
            maxfun=max_iter,
            rhobeg=0.5,
            rhoend=0.02,
            objfun_has_noise=True,
            seek_global_minimum=True,
            print_progress=False,
        )
        f_min_B = results.f
        x_min_B = results.x
        penalty_B.append(f_min_B)
        actions_B.append(x_min_B)
    except AssertionError:
        print("PyBOBYQA failed, adding penalty...")
        penalty_B.append(1.)
        actions_B.append(dtau0)
    #---------------------------------------------------------------------------------------"""
    
    # -------------------- Nelder-Mead and Powell Scipy ---------------------------------
  
    results_NM = optimize.minimize(minimizing_objective, dtau0, bounds=bounds_2, method='Nelder-Mead', options={'disp': False, 'maxfev' : max_iter})
    f_min_NM = results_NM.fun
    x_min_NM = results_NM.x
    penalty_NM.append(f_min_NM)
    actions_NM.append(x_min_NM)
    
    results_P = optimize.minimize(minimizing_objective, dtau0, bounds=bounds_2, method='Powell', options={'disp': False, 'maxfev' : max_iter})
    f_min_P = results_P.fun
    x_min_P = results_P.x
    penalty_P.append(f_min_P)
    actions_P.append(x_min_P)
    #---------------------------------------------------------------------------------------"""
    
    # -------------------- Bayesian ---------------------------------------------------------
    optimiser = BayesianOptimization(
        f=bayesian_objective,
        pbounds=p_bounds,
        verbose=1,
        random_state=10,
    )
    #Probe some initial points, lazy=True means that they are the first to be explored when maximizing
    optimiser.probe(
        params=dtau0,
        lazy=True,
    )
    optimiser.maximize(init_points=30, n_iter=max_iter, acq="ei", xi=1e-1)
    f_min_Bay = np.abs(optimiser.max['target'])
    [*keys],[*values] = zip(*optimiser.max['params'].items()) #merge dtau parameters into a single array 
    x_min_Bay = np.array(values)
    penalty_Bay.append(f_min_Bay)
    actions_Bay.append(x_min_Bay)
    # ---------------------------------------------------------------------------------------
    
#Save the data to a dictionary
IC_data = {'PyBOBYQA_loss' : np.array(penalty_B), 'PyBOBYQA_actions' : np.array(actions_B),
           'NelderMead_loss' : np.array(penalty_NM), 'NelderMead_actions' : np.array(actions_NM),
           'Powell_loss' : np.array(penalty_P), 'Powell_actions' : np.array(actions_P),
           'Bayesian_loss' : np.array(penalty_Bay), 'Bayesian_actions' : np.array(actions_Bay)
           }    
  
filehandler = open("nn_in_optimisation_results/IC_data_{}_{}_training_epochs_{}_ICs_{}_maxiter".format(load_str, n_epochs, n_t, max_iter),"wb")
pickle.dump(IC_data,filehandler)
filehandler.close()
  
#Option to remove failed cases from PyBOBYQA
penalty_B = [ele for ele in penalty_B if ele < 1.0]

#Make histograms for cumulative sum plots
# evaluate the histograms
values_B, base_B = np.histogram(penalty_B, bins=50)
values_NM, base_NM = np.histogram(penalty_NM, bins=50)
values_P, base_P = np.histogram(penalty_P, bins=50)
values_Bay, base_Bay = np.histogram(penalty_Bay, bins=50)
#evaluate the cumulative distributions
cumulative_B = np.cumsum(values_B/n_t)
cumulative_NM = np.cumsum(values_NM/n_t)
cumulative_P = np.cumsum(values_P/n_t)
cumulative_Bay = np.cumsum(values_Bay/n_t)
    

#Plot the different cumulative distributions
fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(10, 7))
ax.plot(base_B[:-1], cumulative_B, c='blue', label='pyBOBYQA')
ax.plot(base_NM[:-1], cumulative_NM, c='green', label='Nelder-Mead')
ax.plot(base_P[:-1], cumulative_P, c='red', label='Powell')
ax.plot(base_Bay[:-1], cumulative_Bay, c='purple', label='Bayesian')
ax.legend(prop={'size': 14})
ax.yaxis.label.set_size(17)
ax.xaxis.label.set_size(17)
ax.set_ylabel("Fraction")
ax.set_xlabel("Value")
ax.tick_params(axis='both',labelsize=14)

# Serialize and save data
if saveplot:
    fig.savefig(
            "nn_in_optimisation_results/cumsum_plot_IC_data_{}_{}_training_epochs_{}_ICs_{}_maxiter.png".format(load_str, n_epochs, n_t, max_iter),
            dpi=250,
        )


    
    
