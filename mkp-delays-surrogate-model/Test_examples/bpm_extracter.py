"""
Script to extract the pickled BPM positions from the runs with the MKP delay optimiser and plot the injection oscillations 
"""
import matplotlib.pyplot as plt
import numpy as np
import pickle
import datetime

saveplot = True
use_last_optimisation = True

#Important time stamps from where to extract relevant data
TIMESTAMP = "2022-04-11_15-08-22.107605" #Elias first run 
TIMESTAMP2 = "2022-04-20_10-50-05.998873"  #Francesco's run, but the best optimised case doesn't have proper oscillation data 

#Unpickle the data 
with open("../plots_and_data/actors_mkp_delays_log_data_{}.pickle".format(TIMESTAMP), "rb") as handle:
    data = pickle.load(handle)
print("\nData is: {}".format(data))
with open("../plots_and_data/actors_mkp_delays_log_data_{}.pickle".format(TIMESTAMP2), "rb") as handle:
    data2 = pickle.load(handle)

#Retrieve index of best optimisation result 
index_min = np.argmax(data["out"])  #as the rewards contain a minus sign 
index_min2 = 240  #one of the best runs for Francesco 

#Plot the BPM positions for injected and circulating bunches 
plt.ion()
fig, axis = plt.subplots(nrows=2, ncols=1, sharex=True, figsize=(8, 8))   #before optimisation and after 
axis[0].plot(data["bpm_pos"][0][0], label="Injected")
axis[0].plot(data["bpm_pos"][0][1], label="Circulating")
axis[0].set_ylim([-8., 10])
axis[0].set_ylabel("x [mm]")
axis[0].legend(loc=4, prop={'size': 14})
axis[0].yaxis.label.set_size(17)
axis[0].xaxis.label.set_size(17)
#plt.xticks(fontsize=14)  
#plt.yticks(fontsize=14)
axis[0].tick_params(axis='both',labelsize=14)
axis[0].text(190, 8.5, "Before optimisation", weight='bold', fontsize=16)
if use_last_optimisation:
    axis[1].plot(data2["bpm_pos"][index_min2][0], label="Injected")
    axis[1].plot(data2["bpm_pos"][index_min2][1], label="Circulating")
    opt_str = 'last_optised_settings_200ns'
else:
    axis[1].plot(data["bpm_pos"][index_min][0], label="Injected")
    axis[1].plot(data["bpm_pos"][index_min][1], label="Circulating")
    opt_str = 'first_optimised_settings_225ns'
axis[1].set_ylabel("x [mm]")
axis[1].set_xlabel("#turns")
axis[1].set_ylim([-8., 10])
axis[1].yaxis.label.set_size(17)
axis[1].xaxis.label.set_size(17)
axis[1].tick_params(axis='both',labelsize=14)
axis[1].text(195, 8.5, "After optimisation", weight='bold', fontsize=16)
plt.tight_layout()
fig.canvas.draw()

if saveplot:
    fig.savefig(
            "plots/injection_oscillations_{}_{}.png".format(TIMESTAMP, opt_str),
            dpi=250,
        )