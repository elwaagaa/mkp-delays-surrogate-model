"""
Script to generate, train and test neural network from the SPS kicker optimisation data, without validation
"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import torch
from torch import nn
from torch.utils.data import Dataset, DataLoader
import sklearn
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.datasets import load_boston
import pickle
import ast 

use_own_data = True #use own data (dim(X) = 923x9), or Boston dataset from scikit learn (dim(X) = 506x13)

if use_own_data == True:
    n_features = 9
    scale_data_bool = False #for own data, doesn't seem to matter as much as input data is already normalised 
    save_str = 'delay_model'
    plt_str = 'MKP delays neural network'
else:
    n_features = 13
    scale_data_bool = True
    save_str = 'boston_model'
    plt_str = 'Boston housing model'

#Set some initial parameters
n_epochs = 1500
batch_size = 64  #originally 64
learning_rate = 0.015

#Flag if any anomalies occur
#torch.autograd.set_detect_anomaly(True)

#method to create custom dataset class
class kickerDataset(torch.utils.data.Dataset):
#Prepare the dataset for regression
  def __init__(self, X, y, scale_data=scale_data_bool):
    if not torch.is_tensor(X) and not torch.is_tensor(y):
      # Apply scaling if necessary
      if scale_data:
          X = StandardScaler().fit_transform(X)
      self.X = torch.from_numpy(X).float()
      self.y = torch.from_numpy(y).float().unsqueeze(-1)  #also add right dimensionality to tensor

  def __len__(self):
      return len(self.X)

  def __getitem__(self, i):
      return self.X[i], self.y[i]


#Multilayer Perceptron method for regression
class MLP(nn.Module):
  def __init__(self, num_features):
    super().__init__()
    self.flatten = nn.Flatten()
    self.layers = nn.Sequential(
      nn.Linear(num_features, 50),
      #nn.ReLU(),
      #nn.Linear(50, 60),
      #nn.ReLU(),
      #nn.Linear(60, 50),
      nn.ReLU(),
      nn.Linear(50, 40),
      nn.ReLU(),
      nn.Linear(40, 30),
      nn.ReLU(),
      nn.Linear(30, 1)
    )
    self.apply(self._init_weights)

  def forward(self, x):
  #Forward pass
    #x = self.flatten(x)  #probably not needed as we already have contiguous arrays
    return self.layers(x)

  #Method to initialize weights
  def _init_weights(self, module):
      if isinstance(module, nn.Linear):
          module.weight.data.normal_(mean=0.0, std=1e-4)
          if module.bias is not None:
              module.bias.data.zero_()
              

if __name__ == '__main__':
    
    #------------ Prepare kicker dataset for pytorch, or use default Boston dataset ------------------ 
    if use_own_data:
       df = pd.read_csv("data_actions_rewards.csv")
       X_raw = df["actions"]
       X = np.empty((0,9), float)
       for ele in X_raw:
           #remove all double white spaces, and convert string to numpy array, then append to matrix 
           ele = " ".join(ele.split())  
           ele = ele.replace('[ ', '[')
           ele = ele.replace(' ]', ']')
           ele = ele.replace(' ', ',')
           ele = np.array(ast.literal_eval(ele))
           X = np.vstack((X, ele))
        
       y = df["out"].to_numpy()
       
       #Save data to serialized file 
       loaded_data = {}
       loaded_data['X'] = X
       loaded_data['y'] = y
       filename = 'data_actions_rewards'
       outfile = open(filename,'wb')
       pickle.dump(loaded_data, outfile)
       outfile.close()
    else:
       X, y = load_boston(return_X_y=True)
     #-----------------------------------------------------------------
    
    #Divide dataset into training, validation and test
    x_train, x_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=4)
    
    
    #Initialize dataset
    training_data = kickerDataset(x_train, y_train)
    test_data = kickerDataset(x_test, y_test)
       
    # Create data loaders.
    train_dataloader = DataLoader(training_data, batch_size=batch_size)
    test_dataloader = DataLoader(test_data, batch_size=batch_size)
    

    for X, y in test_dataloader:
        print(f"Shape of X: {X.shape} {X.dtype}")
        print(f"Shape of y: {y.shape} {y.dtype}")
        break
   
    # Get cpu or gpu device for training.
    device = "cuda" if torch.cuda.is_available() else "cpu"
    print(f"Using {device} device") 
   
    
    # Initialize the MLP
    model = MLP(n_features).to(device)

    # Define the loss function and optimizer
    loss_fn = nn.MSELoss() #originially squared L2 norm  (MSELoss)
    optimizer = torch.optim.SGD(model.parameters(), lr=learning_rate)
    
    #Store the losses in array
    train_losses = []
    
    
    #Define functions to train and test data 
    def train(dataloader, model, loss_fn, optimizer):
        size = len(dataloader.dataset)
        model.train()
        for batch, (X, y) in enumerate(dataloader):
            X, y = X.to(device), y.to(device)
    
            # Compute prediction error
            pred = model(X)
            loss = loss_fn(pred, y)
    
            # Backpropagation
            optimizer.zero_grad()
            loss.backward()
                    
            nn.utils.clip_grad_value_(model.parameters(), clip_value=1.0) #Gradient Value Clipping
            optimizer.step()
            
        train_losses.append(loss)
        print("Loss is: {:.2f}\n".format(loss.item()))

    
    def test(dataloader, model, loss_fn):
        size = len(dataloader.dataset)
        num_batches = len(dataloader)
        model.eval()
        test_loss, correct = 0, 0
        with torch.no_grad():
            for X, y in dataloader:
                X, y = X.to(device), y.to(device)
                pred = model(X)
                test_loss += loss_fn(pred, y).item()
                correct += (pred.argmax(1) == y).type(torch.float).sum().item()
        test_loss /= num_batches
        correct /= size


    #"""
    #Train across epochs
    for t in range(n_epochs):
        print(f"------------ Epoch {t+1} -------------")
        train(train_dataloader, model, loss_fn, optimizer)
        test(test_dataloader, model, loss_fn)
    print("Done!")
    
    
    torch.save(model.state_dict(), "{}.pth".format(save_str))
    print("\nSaved PyTorch Model State to {}.pth".format(save_str))
    #"""
    
    #Create array of epochs, and convert tensor to floats
    epochs = np.arange(0, n_epochs)
    train_losses_np = []
    for ele in train_losses:
        ele = ele.item()
        train_losses_np.append(ele) 
    
    plt.figure()
    plt.plot(epochs, train_losses_np)
    plt.xlabel("#epochs")
    plt.ylabel("Loss")
    plt.yscale('log')
    plt.suptitle(plt_str)
    plt.show()
