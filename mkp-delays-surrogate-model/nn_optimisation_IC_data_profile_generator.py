
"""
Script to load pickled data generated from nn_optimisation_IC_sweeper, and create data profile plots
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import pickle

#Choose model to load depending on #training_epochs
n_epochs = 200  #choose number of epochs of model to load from
use_normalised_input = True
use_trained_only = False #use nn models which have been fully trained on the full dataset, not validated
n_t = 100  #total number of IC:s to test
saveplot = True

max_iter = [100, 150, 250, 375, 500] #maximum number of iterations for each optimiser  

#Choose correct string to load model 
if use_normalised_input :
    norm_str = '_normalised_input'
else: 
    norm_str = ''    
if use_trained_only:
    train_str = '_training_only'
else:
    train_str = ''
    
#Iterate over the different scenarios of IC
n = len(max_iter)
IC_data_all = []
cum_sums_y_B = []
cum_sums_x_B = []
cum_sums_y_NM = []
cum_sums_x_NM = []
cum_sums_y_P = []
cum_sums_x_P = []
cum_sums_y_Bay = []
cum_sums_x_Bay = []
for ele in max_iter:
    load_str = 'nn_in_optimisation_results/IC_data_mkp_delay_model{}{}_{}_training_epochs_{}_ICs_{}_maxiter'.format(norm_str, train_str, n_epochs, n_t, ele)
    
    #Unpickle the data
    with open(load_str, "rb") as handle:
        IC_data = pickle.load(handle)
    IC_data_all.append(IC_data)
    
    penalty_B = IC_data['PyBOBYQA_loss']
    penalty_NM = IC_data['NelderMead_loss']
    penalty_P = IC_data['Powell_loss']
    penalty_Bay = IC_data['Bayesian_loss']    
    
    #Option to remove failed cases from PyBOBYQA
    penalty_B = [ele for ele in penalty_B if ele < 1.0]
    
    #Make histograms for cumulative sum plots
    # evaluate the histograms
    values_B, base_B = np.histogram(penalty_B, bins=50)
    values_NM, base_NM = np.histogram(penalty_NM, bins=50)
    values_P, base_P = np.histogram(penalty_P, bins=50)
    values_Bay, base_Bay = np.histogram(penalty_Bay, bins=50)
    #evaluate the cumulative distributions
    cumulative_B = np.cumsum(values_B/n_t)
    cumulative_NM = np.cumsum(values_NM/n_t)
    cumulative_P = np.cumsum(values_P/n_t)
    cumulative_Bay = np.cumsum(values_Bay/n_t)

    #Append all the x- and y-values of the cumsums for each algorithm
    cum_sums_y_B.append(cumulative_B)
    cum_sums_x_B.append(base_B[:-1])
    cum_sums_y_NM.append(cumulative_NM)
    cum_sums_x_NM.append(base_NM[:-1])    
    cum_sums_y_P.append(cumulative_P)
    cum_sums_x_P.append(base_P[:-1])
    cum_sums_y_Bay.append(cumulative_Bay)
    cum_sums_x_Bay.append(base_Bay[:-1])
    
    
#Find index of alpha corresponding successful to successful value (fraction of at least 50%)
data_prof_B = [] 
ind_B = np.where(cum_sums_y_B[0] <= 0.5)[0][-1]
alpha_B = cum_sums_x_B[0][ind_B]
data_prof_B.append(cum_sums_y_B[0][ind_B])  #the first data profile element 

data_prof_NM = [] 
ind_NM = np.where(cum_sums_y_NM[0] < 0.5)[0][-1]
alpha_NM = cum_sums_x_NM[0][ind_NM]
data_prof_NM.append(cum_sums_y_NM[0][ind_NM]) 

data_prof_P = [] 
ind_P = np.where(cum_sums_y_P[0] < 0.5)[0][-1]
alpha_P = cum_sums_x_P[0][ind_P]
data_prof_P.append(cum_sums_y_P[0][ind_P]) 

data_prof_Bay = [] 
ind_Bay = np.where(cum_sums_y_Bay[0] < 0.5)[0][-1]
alpha_Bay = cum_sums_x_Bay[0][ind_Bay]
data_prof_Bay.append(cum_sums_y_Bay[0][ind_Bay]) 

#Iterate over the different budgets (max_iter) to see the fraction that surpasses this threshold 
for i in range(1, len(max_iter)):
    ind_B_new = np.where(cum_sums_x_B[i] <= alpha_B)[0][-1]
    data_prof_B.append(cum_sums_y_B[i][ind_B_new])  #find corresponding fraction (y-value) for the same alpha
    #print(cum_sums_y_B[i][ind_B_new])
    ind_NM_new = np.where(cum_sums_x_NM[i] <= alpha_NM)[0][-1]
    data_prof_NM.append(cum_sums_y_NM[i][ind_NM_new])
    
    ind_P_new = np.where(cum_sums_x_P[i] <= alpha_P)[0][-1]
    data_prof_P.append(cum_sums_y_P[i][ind_P_new])
    
    ind_Bay_new = np.where(cum_sums_x_Bay[i] <= alpha_Bay)[0][-1]
    data_prof_Bay.append(cum_sums_y_Bay[i][ind_Bay_new])
    
#Plot these data points 
fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(17, 7))
ax.step(max_iter, data_prof_B, c='blue', label='pyBOBYQA', linewidth=5, marker="*", markersize=34, linestyle="dotted")
ax.step(max_iter, data_prof_NM, c='green', label='Nelder-Mead', linewidth=5, marker="*", markersize=34, linestyle="dotted")
ax.step(max_iter, data_prof_P, c='red', label='Powell', linewidth=5, marker="*", markersize=34, linestyle="dotted")
ax.step(max_iter, data_prof_Bay, c='purple', label='Bayesian', linewidth=5, marker="*", markersize=34, linestyle="dotted")
ax.legend(loc='lower right', prop={'size': 22})
ax.yaxis.label.set_size(26)
ax.xaxis.label.set_size(26)
ax.set_ylabel("Data profile $d(k)$")
ax.set_xlabel("Function evaluation budget $k$")
ax.tick_params(axis='both',labelsize=22)
#ax.set_xlim([0.46, 0.74])

# Serialize and save data
if saveplot:
    fig.savefig(
            "nn_in_optimisation_results/data_profile_mkp_delay_model{}{}_{}_training_epochs_{}_ICs.png".format(norm_str, train_str, n_epochs, n_t),
            dpi=250,
        )

