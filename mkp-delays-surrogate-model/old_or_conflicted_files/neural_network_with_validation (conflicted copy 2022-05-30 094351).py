"""
Script to generate, train and test neural network from the SPS kicker optimisation data

This version also includes validation of dataset 
"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import torch
from torch import nn
from torch.utils.data import Dataset, DataLoader
import sklearn
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import MaxAbsScaler  #to normalize output data 
from sklearn.model_selection import train_test_split
from sklearn.datasets import load_boston
import pickle
import ast 

use_own_data = True #use own data (dim(X) = 923x9), or Boston dataset from scikit learn (dim(X) = 506x13)
scale_y = True #if also scaling the output data
log_scale_y = True #perform normalized log scale transform
do_gradual_training = True #if true, learning rate will change throughout 
normalised_input = True  #to normalise input from [-1, 1] --> [0, 1]
save_plot = True 
load_only_kept_beam = False #load data only where the beam was not lost (no high penalty plateau)
use_pickled_data = False #if already pickled, do not use if only kept beam is used 

#Set some initial parameters
n_epochs = 150
batch_size = 64  #originally 64
learning_rate = 4e-4 
torch.manual_seed(4)    # so is reproducible

if use_own_data == True:
    n_features = 9
    scale_data_bool = normalised_input
    if normalised_input:
        save_str = 'mkp_delay_model_normalised_input'
    else:    
        save_str = 'mkp_delay_model'
    plt_str = 'MKP delays neural network'
else:
    n_features = 13
    scale_data_bool = True
    save_str = 'boston_model'
    plt_str = 'Boston housing model'

#Flag if any anomalies occur
torch.autograd.set_detect_anomaly(True)

#method to create custom dataset class
class kickerDataset(torch.utils.data.Dataset):
#Prepare the dataset for regression
  def __init__(self, X, y, scale_data=scale_data_bool):
    if not torch.is_tensor(X) and not torch.is_tensor(y):
      # Apply scaling if necessary
      if scale_data:
          #X = StandardScaler().fit_transform(X)   #suitable for Boston dataset, but not for our case 
          X = (X + 1.0)/2  #maps [-1, 1] --> [0, 1]
          #print(np.min(X))
      self.X = torch.from_numpy(X).float()
      self.y = torch.from_numpy(y).float().unsqueeze(-1)  #also add right dimensionality to tensor

  def __len__(self):
      return len(self.X)

  def __getitem__(self, i):
      return self.X[i], self.y[i]


#Multilayer Perceptron method for regression
class MLP(nn.Module):
  def __init__(self, num_features):
    super().__init__()
    self.flatten = nn.Flatten()
    self.layers = nn.Sequential(
      nn.Linear(num_features, 200),
      nn.BatchNorm1d(200),
      nn.ReLU(),   #default: ReLU
      nn.Linear(200, 150),
      nn.BatchNorm1d(150),
      nn.ReLU(),
      nn.Linear(150, 100),
      nn.BatchNorm1d(100),
      nn.ReLU(),
      nn.Linear(100, 1)
    )
    self.apply(self._init_weights)

  def forward(self, x):
  #Forward pass
    #x = self.flatten(x)  #probably not needed as we already have contiguous arrays
    return self.layers(x)

  #Method to initialize weights
  def _init_weights(self, module):
      if isinstance(module, nn.Linear):
          module.weight.data.normal_(mean=0.0, std=1e-4)
          if module.bias is not None:
              module.bias.data.zero_()
              

if __name__ == '__main__':
    
    #------------ Prepare kicker dataset for pytorch, or use default Boston dataset ------------------ 
    if use_own_data:
        if use_pickled_data:
            a = pickle.load(open('data_actions_rewards', 'rb'))
            X = a['X']
            y = a['y']
        else: 
            if load_only_kept_beam:
                df = pd.read_csv("data_actions_rewards_only_kept_beam.csv")
            else:
                df = pd.read_csv("data_actions_rewards.csv")
            X_raw = df["actions"]
            X = np.empty((0,9), float)
            for ele in X_raw:
               #remove all double white spaces, and convert string to numpy array, then append to matrix 
               ele = " ".join(ele.split())  
               ele = ele.replace('[ ', '[')
               ele = ele.replace(' ]', ']')
               ele = ele.replace(' ', ',')
               ele = np.array(ast.literal_eval(ele))
               X = np.vstack((X, ele))
            
            y = df["out"].to_numpy()
           
            #Save data to serialized file 
            loaded_data = {}
            loaded_data['X'] = X
            loaded_data['y'] = y
            filename = 'data_actions_rewards'
            outfile = open(filename,'wb')
            pickle.dump(loaded_data, outfile)
            outfile.close()
    else:
       X, y = load_boston(return_X_y=True)
     #-----------------------------------------------------------------
    
    #Transform the output data to lie in range [0, 1]
    if scale_y:
        y = y.reshape(-1, 1)
        if log_scale_y:        
            y = np.log(np.abs(y))  #.reshape(-1)
        scaler = MaxAbsScaler()
        scaler.fit(y)
        y = np.abs(scaler.transform(y).reshape(-1)) #transform, then back to correct shape 
    
    #Divide dataset into training and validation
    #x_train, x_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=4)
    
    #--------------- following Rebeccas example ------------------
    # Train - Test Validate split using scikit-learn
    x_trainval, x_test, y_trainval, y_test = train_test_split(X, y, test_size=0.1, random_state=21)# Split train into train-val
    x_train, x_val, y_train, y_val = train_test_split(x_trainval, y_trainval, test_size=0.25, random_state=52)
    #--------------------------------------------------------------
    
    #Initialize dataset
    training_data = kickerDataset(x_train, y_train)
    val_data = kickerDataset(x_val, y_val)
    test_data = kickerDataset(x_test, y_test)
       
    # Create data loaders.
    train_dataloader = DataLoader(training_data, batch_size=batch_size)
    val_dataloader = DataLoader(val_data, batch_size=1)
    test_dataloader = DataLoader(test_data, batch_size=1)

    """
    for X, y in test_dataloader:
        print(f"Shape of X: {X.shape} {X.dtype}")
        print(f"Shape of y: {y.shape} {y.dtype}")
        break
   
    # Get cpu or gpu device for training.
    device = "cuda" if torch.cuda.is_available() else "cpu"
    print(f"Using {device} device") 
   
    # Initialize the MLP
    model = MLP(n_features).to(device)

    # Define the loss function and optimizer
    loss_fn = nn.MSELoss() #originially squared L2 norm  (MSELoss)
    optimizer = torch.optim.SGD(model.parameters(), lr=learning_rate, weight_decay=1e-5)
    
    #Store the losses in array
    train_losses = []
    loss_stats = {
        'train': [],
        "val": []
    }
    
    #Define functions to train, test and validate data 
    def train(dataloader, model, loss_fn, optimizer):
        size = len(dataloader.dataset)
        num_batches = len(dataloader)
        train_epoch_loss = 0
        model.train()
        for batch, (X, y) in enumerate(dataloader):
            X, y = X.to(device), y.to(device)
    
            # Compute prediction error
            pred = model(X)
            loss = loss_fn(pred, y)
    
            # Backpropagation
            optimizer.zero_grad()
            loss.backward()
                    
            #if not scale_y:
            nn.utils.clip_grad_value_(model.parameters(), clip_value=1.0) #Gradient Value Clipping
            optimizer.step()
            train_epoch_loss += loss.item()
            
        train_epoch_loss /= num_batches
        loss_stats['train'].append(train_epoch_loss)
        print("Train loss is: {:.2e}".format(train_epoch_loss))

    
    def validate(dataloader, model, loss_fn):
        size = len(dataloader.dataset)
        num_batches = len(dataloader)
        model.eval()
        val_loss, correct = 0, 0
        with torch.no_grad():
            for X, y in dataloader:
                X, y = X.to(device), y.to(device)
                pred = model(X)
                #print("Validated prediction: {}".format(pred))
                val_loss += loss_fn(pred, y).item()
                correct += (pred.argmax(1) == y).type(torch.float).sum().item()
        val_loss /= num_batches
        correct /= size
        loss_stats['val'].append(val_loss)   
        print("Test loss is: {:.2e}\n".format(val_loss))

    #-------------- Train across epochs, and validate ----------------------------
    for t in range(n_epochs):
        print(f"------------ Epoch {t+1} -------------")
        if do_gradual_training:
            if t == 200:
                for g in optimizer.param_groups:
                    g['lr'] = learning_rate/2
            if t == 300:
                for g in optimizer.param_groups:
                    g['lr'] = learning_rate/4
        train(train_dataloader, model, loss_fn, optimizer)
        validate(val_dataloader, model, loss_fn)
    print("Done!")
    
    #Test the trained model 
    y_pred_list = []
    y_real = []
    count = 0
    with torch.no_grad():
        model.eval()
        for X_batch, _ in test_dataloader:
            X_batch = X_batch.to(device)
            y_test_pred = model(X_batch)
            y_r = test_data.y.numpy()[count][0] #compare with actual value from test dataset 
            y_real.append(y_r)
            print("Predicted: {:.4f}, actual: {:.4f}".format(y_test_pred.item(), y_r))
            y_pred_list.append(y_test_pred.cpu().numpy())
            count += 1 
        y_pred_list = np.array([a.squeeze().tolist() for a in y_pred_list]).flatten()
        y_bar = np.array(y_pred_list).flatten()

    
    
    torch.save(model.state_dict(), "nn_models/{}_{}_training_epochs.pth".format(save_str, n_epochs))
    print("\nSaved PyTorch Model State to {}_{}_epochs.pth".format(save_str, n_epochs))
    
    
    train_val_loss_df = pd.DataFrame.from_dict(loss_stats).reset_index().melt(id_vars=['index']).rename(columns={"index":"epochs"})
    
    
    #Perform goodness of fit 
    y_mean = np.mean(y_real)
    SS_res = np.sum((y_real - y_bar)**2)
    SS_tot = np.sum((y_real - y_mean)**2)
    R2 = 1 - SS_res/SS_tot
    print("\nR2 is {:.2f}".format(R2))
    
    #Plot training and validation
    fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(10, 7))
    sns.lineplot(data=train_val_loss_df, x = "epochs", y="value", hue="variable")   
    ax.legend(loc=1, prop={'size': 14})
    ax.set_yscale('log')
    ax.yaxis.label.set_size(17)
    ax.xaxis.label.set_size(17)
    ax.tick_params(axis='both',labelsize=14)
    
    #Plot testing 
    fig2, ax2 = plt.subplots(nrows=1, ncols=1, figsize=(10, 7))
    ax2.plot(y_real, y_bar, marker="*", markersize=8,  linestyle='None')
    #ax2.legend(loc=1, prop={'size': 14})
    ax2.yaxis.label.set_size(17)
    ax2.xaxis.label.set_size(17)
    ax2.set_ylabel("$\\bar{y}$")
    ax2.set_xlabel("$y_{actual}$")
    ax2.tick_params(axis='both',labelsize=14)
   

    if save_plot:
        fig.savefig(
                "nn_result_plots/nn_training_norm_log_output_relu_{}_{}_training_epochs.png".format(save_str, n_epochs),
                dpi=250,
            )
        fig2.savefig(
                "nn_result_plots/nn_actual_vs_predicted_norm_log_output_relu_{}_{}_training_epochs.png".format(save_str, n_epochs),
                dpi=250,
            )
     #"""
