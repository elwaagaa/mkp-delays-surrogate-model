"""
Optimisation script for trained neural network with MKP kicker delays from SPS  

Uses nn models whose input are normalised from [-1, 1] --> [0, 1], and whose output is normalised of the logarithm to [0, 1]
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import torch
from torch import nn
import pickle
from scipy import optimize
import pybobyqa
from neural_network_with_validation import MLP

#Choose model to load depending on #training_epochs
n_epochs = 150  #choose number of epochs of model to load from
n_features = 9  #mkp delays: 8 individual shift, 1 general shift 
saveplot = True
use_trained_only = False #use nn models which have been fully trained on the full dataset, not validated
use_bobyqa = True #otherwise use scipy 
scipy_method = 'Powell'   #'Nelder-Mead'  #if scipy, what method to use

model = MLP(n_features)
if use_trained_only:
    load_str = 'mkp_delay_model_normalised_input_training_only'
else:    
    load_str = 'mkp_delay_model_normalised_input'

model.load_state_dict(torch.load("nn_models/{}_{}_training_epochs.pth".format(load_str, n_epochs)))

#Initiate action and penalty 
actions_all = []
penalty = []
dtau0 = np.zeros(9)  #starting guess

# Define function to minimise, converting floats to tensors for the model 
# also generates plots and saves data
def minimizing_objective(dtau):
    if not torch.is_tensor(dtau):
        actions = torch.from_numpy(dtau).float()  #convert to tensor
    model.eval()
    with torch.no_grad():
        actions = torch.unsqueeze(actions, 0)  #unsqueeze to right dimensions
        loss = model(actions)  #evaluate with trained model 
    penalty.append(loss.item())
    actions_all.append(np.array(actions).flatten())

    return loss.item()


if use_bobyqa:
    #"""
    # ------------ Optimize with PyBOBYQA ----------------------------------------
    opt_model = 'bobyqa'
    lim_length = 9 #all 8 individual shifts + general shift 
    upper_lim = np.ones(lim_length)
    lower_lim = -1 * np.ones(lim_length)
    bounds = (lower_lim, upper_lim)
    
    results = pybobyqa.solve(
        minimizing_objective,
        dtau0,
        bounds=bounds,
        maxfun=10000,
        rhobeg=0.5,
        rhoend=0.02,
        objfun_has_noise=True,
        seek_global_minimum=True,
        print_progress=True,
    )
    print(results)
    f_min = results.f
    x_min = results.x
    #---------------------------------------------------------------------------------------"""
else:
    #"""
    # -------------------- Scipy optimisation minimization ---------------------------------
    opt_model = "scipy_{}".format(scipy_method)
    upper_lim_2 = np.ones(9)
    lower_lim_2 = -1*np.ones(9)
    bounds_2 = tuple(zip(lower_lim_2, upper_lim_2))
    results = optimize.minimize(minimizing_objective, dtau0, bounds=bounds_2, method=scipy_method, options={'disp': True})
    f_min = results.fun
    x_min = results.x
    
    print("\n\nResult:")
    print(results)
    #---------------------------------------------------------------------------------------"""


#Optimisation results in dictionary 
opt_results = {'method': opt_model, 'opt_results': results, 'f_min': f_min, 'x_min': x_min, 'all_actions': actions_all, 'all_losses': penalty}

#Plot the results 
fig, axis = plt.subplots(nrows=2, ncols=1, sharex=True, figsize=(14, 8))
axis[0].plot([], [])
axis[1].plot([], [])
axis[0].yaxis.label.set_size(16)
axis[0].tick_params(axis="y", labelsize=13)
axis[1].yaxis.label.set_size(16)
axis[1].xaxis.label.set_size(16)
axis[1].tick_params(axis="x", labelsize=13)
axis[1].tick_params(axis="y", labelsize=13)
legend_names = [
    "Switch 1",
    "Switch 2",
    "Switch 3",
    "Switch 4",
    "Switch 5",
    "Switch 6",
    "Switch 7",
    "Switch 8",
    "General shift",
]
#Plot the actions and loss functions 
for ax in axis:
    ax.cla()
    axis[0].plot(penalty, color='r', marker=10, ms=5)
    axis[0].set(ylabel="Normalised log of loss")
    #axis[0].set_yscale("log")
    # axis[0].set_yticks([5.0, 8.0])  #add extra ticks of BPM positions
    # axis[0].get_yaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
    axis[1].plot(actions_all, label=legend_names, marker=11, ms=5)
    axis[1].set(xlabel="#Iterations", ylabel="Actions")
    axis[1].legend(loc="right", fancybox=True, bbox_to_anchor=(1.125, 0.6))
    plt.subplots_adjust(wspace=0, hspace=0)
    fig.canvas.draw()
    axis[0].get_shared_x_axes().join(
        axis[0], axis[1]
    )  # make the subplots share the x-axis of iterations

# Serialize and save data
if saveplot:
    fig.savefig(
            "nn_in_optimisation_results/actors_plot_{}_{}_{}_training_epochs.png".format(load_str, opt_model, n_epochs),
            dpi=250,
        )
    
    #Save optimisation result as dictionary:
    filename = "nn_in_optimisation_results/opt_results_{}_{}_{}_training_epochs".format(load_str, opt_model, n_epochs)
    outfile = open(filename,'wb')
    pickle.dump(opt_results, outfile)
    outfile.close()
