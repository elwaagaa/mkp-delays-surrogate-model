#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Script to load pickled data generated from nn_optimisation_IC_sweeper, and recreate the plots
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import pickle

#Choose model to load depending on #training_epochs
n_epochs = 150  #choose number of epochs of model to load from
use_normalised_input = True
use_trained_only = False #use nn models which have been fully trained on the full dataset, not validated
max_iter = 100 #maximum number of iterations for each optimiser  
n_t = 100  #total number of IC:s to test
saveplot = True

#Choose correct string to load model 
if use_normalised_input :
    norm_str = '_normalised_input'
else: 
    norm_str = ''    
if use_trained_only:
    train_str = '_training_only'
else:
    train_str = ''
load_str = 'nn_in_optimisation_results/IC_data_mkp_delay_model{}{}_{}_training_epochs_{}_ICs_{}_maxiter'.format(norm_str, train_str, n_epochs, n_t, max_iter)

#Unpickle the data
with open(load_str, "rb") as handle:
    data = pickle.load(handle)