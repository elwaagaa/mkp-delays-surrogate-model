#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Script to load pickled data generated from nn_optimisation_IC_sweeper, and recreate the plots
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import pickle

#Choose model to load depending on #training_epochs
n_epochs = 150  #choose number of epochs of model to load from
use_normalised_input = True
use_trained_only = False #use nn models which have been fully trained on the full dataset, not validated
max_iter = 300 #maximum number of iterations for each optimiser  
n_t = 100  #total number of IC:s to test
saveplot = False

#For lowest number of maxiter = 100, at what value does the best optimiser reach 50% in the cumsum plot? (i.e. 50% successful)
#for maxiter = 100, at 50% of the fun_evals of pybobyqa are below  0.6056608319282532 --> set this value as tolerance
#tol = 0.6056608319282532


#Choose correct string to load model 
if use_normalised_input :
    norm_str = '_normalised_input'
else: 
    norm_str = ''    
if use_trained_only:
    train_str = '_training_only'
else:
    train_str = ''
load_str = 'nn_in_optimisation_results/IC_data_mkp_delay_model{}{}_{}_training_epochs_{}_ICs_{}_maxiter'.format(norm_str, train_str, n_epochs, n_t, max_iter)

#Unpickle the data
with open(load_str, "rb") as handle:
    IC_data = pickle.load(handle)
    
penalty_B = IC_data['PyBOBYQA_loss']
penalty_NM = IC_data['NelderMead_loss']
penalty_P = IC_data['Powell_loss']
penalty_Bay = IC_data['Bayesian_loss']    

#Option to remove failed cases from PyBOBYQA
penalty_B = [ele for ele in penalty_B if ele < 1.0]

#Make histograms for cumulative sum plots
# evaluate the histograms
values_B, base_B = np.histogram(penalty_B, bins=50)
values_NM, base_NM = np.histogram(penalty_NM, bins=50)
values_P, base_P = np.histogram(penalty_P, bins=50)
values_Bay, base_Bay = np.histogram(penalty_Bay, bins=50)
#evaluate the cumulative distributions
cumulative_B = np.cumsum(values_B/n_t)
cumulative_NM = np.cumsum(values_NM/n_t)
cumulative_P = np.cumsum(values_P/n_t)
cumulative_Bay = np.cumsum(values_Bay/n_t)
    

#Plot the different cumulative distributions
fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(10, 7))
ax.plot(base_B[:-1], cumulative_B, c='blue', label='pyBOBYQA')
ax.plot(base_NM[:-1], cumulative_NM, c='green', label='Nelder-Mead')
ax.plot(base_P[:-1], cumulative_P, c='red', label='Powell')
ax.plot(base_Bay[:-1], cumulative_Bay, c='purple', label='Bayesian')
ax.legend(prop={'size': 16})
ax.yaxis.label.set_size(19)
ax.xaxis.label.set_size(19)
ax.set_ylabel("Fraction")
ax.set_xlabel("Value")
ax.tick_params(axis='both',labelsize=15)
ax.set_xlim([0.48, 0.74])


# Serialize and save data
if saveplot:
    fig.savefig(
            "nn_in_optimisation_results/cumsum_plot_IC_data_mkp_delay_model{}{}_{}_training_epochs_{}_ICs_{}_maxiter_wideplot.png".format(norm_str, train_str, n_epochs, n_t, max_iter),
            dpi=250,
        )
